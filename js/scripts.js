$(document).ready(function() {
    $("#owl-demo").owlCarousel({
        rtl: true,
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true
    });
    $("#owl-product").owlCarousel({
        rtl: true,
        items: 4,
        loop: true,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        margin: 50,
        nav: true,

        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            800: {
                items: 2,
            },
            1000: {
                items: 3,
            },
            1300: {
                items: 4,
            }
        }
    });
    $(".owl-prev").html('<div class="nav-btn prev-slide"></div>');
    $(".owl-next").html('<div class="nav-btn next-slide"></div>');

    $("#owl-product-2").owlCarousel({
        rtl: true,
        items: 4,
        loop: true,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        margin: 50,
        nav: true,

        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            800: {
                items: 2,
            },
            1000: {
                items: 3,
            },
            1300: {
                items: 4,
            }
        }
    });
    $(".owl-prev").html('<div class="nav-btn prev-slide"></div>');
    $(".owl-next").html('<div class="nav-btn next-slide"></div>');

    $("#owl-marka").owlCarousel({
        rtl: true,
        items: 8,
        loop: true,
        autoplay: true,
        autoplayTimeout: 1000,
        autoplayHoverPause: true,
        margin: 50,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 2,
            },
            600: {
                items: 2,
            },
            800: {
                items: 3,
            },
            1000: {
                items: 5,
            },
            1300: {
                items: 8,
            }
        }
    });
    $(".owl-prev").html('<div class="nav-btn prev-slide"></div>');
    $(".owl-next").html('<div class="nav-btn next-slide"></div>');

});